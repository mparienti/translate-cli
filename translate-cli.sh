#!/bin/bash
#

# Test kotki-cli is install

[ -x "$(command -v kotki-cli)" ] || (echo kotki-cli not found; exit 3)

#
# If jq is installed, this should be dynamic
#
langs=(
    en
    fr
    de
    bg
    cs
    es
    et
    is
    nb
    nn
)


usage () {
    echo "Usage: ${0} lang_from:lang_to 'String to translate'

Translate 'String to translate' argument from lang_from to lang_to language

For example:
${0} fr:de \"Bonjour le monde\"
    " >&2
      exit 3
}

wrong_language () {
    echo "Language not supported
    " >&2
      exit 3
}

is_a_lang () {
    for key in ${langs[*]}
    do
        if [ "$key" == "$1" ]
        then
            echo true
        fi
    done
    echo false
}

models="$1"
to_translate="$2"

# Check arguments count
( [ -z "$1" ] || [ -z "$2" ] || [ ${#models} -ne 5 ] ) && usage

# Extract languages
from=${models:0:2}
to=${models:3:2}

# Test both languages exists
( [ "$(is_a_lang $from)" == "false" ] || [ "$(is_a_lang $to)" == "false" ]) && wrong_language

if [ "${from}" != "en" ] && [  "${to}" != "en" ]
then
    to_translate=$(kotki-cli -m ${from}"en" -i "$to_translate")
    from=en
fi

kotki-cli -m ${from}${to} -i "$to_translate"
echo
